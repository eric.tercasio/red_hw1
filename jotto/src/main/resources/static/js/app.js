// Game class
class Game {
    constructor(json) {
        this.id = json.id;
        this.user = json.user;
        this.status = json.status;
        this.playerGuesses = json.playerGuesses.reverse();
        this.computerGuesses = json.computerGuesses.reverse();
        this.playerWord = json.playerWord;
        this.computerWord = json.computerWord;
        this.playerGuessList = json.playerGuessList.reverse();
        this.computerGuessList = json.computerGuessList.reverse();
        this.correctCharArray = json.correctCharArray;
        this.incorrectCharArray = json.incorrectCharArray;
        this.computerCorrectCharArray = json.computerCorrectCharArray;
        this.computerIncorrectCharArray = json.computerIncorrectCharArray;

        this.created = json.created && new Date(json.created);
        this.updated = json.updated && new Date(json.updated);

        switch (this.status) {
            case "WIN":
                this.statusText = "Won";
                break;
            case "LOSS":
                this.statusText = "Lost";
                break;
            case "IN_PROGRESS":
                this.statusText = "In progress";
                break;
            default:
                this.statusText = "Unknown";
        }
    }
}

// API functions
function submitGuess(guess, gameId) {
    return new Promise((res, rej) => {
        checkWord(guess)
            .then((result) => {
                if (result === true) {
                    const data = {
                        gid: gameId,
                        guess: guess
                    };
                    return $.ajax("/api", {
                        method: "PUT",
                        contentType: "application/json",
                        data: JSON.stringify(data),
                        success: (data) => {
                            res(new Game(data));
                        },
                        error: (err) => {
                            rej(err.responseText);
                        }
                    });
                } else {
                    rej("Invalid word");
                }
            });
    });

}

function getAllGames() {
    return $.get("/api")
        .then(function (games) {
            return games.map(function (game) {
                return new Game(game)
            });
        });
}

function getOneGame(gameId) {
    return $.get("/api/" + gameId)
        .then(function (game) {
            return new Game(game);
        });
}


function submitLetterStatus(letter, status, gameId) {
//    TODO: ajax call to backend
    const data = {
        gameId: gameId,
        letter: letter,
        state: status
    };

    return new Promise((res, rej) => {
        $.ajax("/api/letter", {
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: (data) => {
                res(new Game(data));
            },
            error: (e) => {
                rej(e.responseText);
            }
        });
    })
}

function createNewGame(word) {
    const data = {
        word: word
    };
    return new Promise((res, rej) => {
        $.ajax("/api",
            {
                contentType: "application/json",
                data: JSON.stringify(data),
                method: "POST",
                success: (data) => {
                    res(new Game(data));
                },
                error: (e) => {
                    rej(e.responseText);
                }
            })
    })
}

function checkWord(word) {
    return $.get("/api/dictionary", {word: word});
}

function showError(error) {
    const snackbarContainer = document.querySelector('#error-notification');
    const data = {message: error, timeout: 5000};
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}