package jotto.model;


import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private User user;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> playerGuesses = new ArrayList<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> computerGuesses = new ArrayList<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Character> correctChar = new ArrayList<>();//Characters marked green(contained in the hidden word) by the player

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Character> incorrectChar = new ArrayList<>();//Characters marked red(not contained in the hidden word) by the player

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Character> computerCorrectChar = new ArrayList<>();//Characters marked green(contained in the hidden word) by the player

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Character> computerIncorrectChar = new ArrayList<>();//Characters marked red(not contained in the hidden word) by the player

    private String playerWord;

    private String computerWord;

    private Status status;

    private Date created;

    private Date updated;

    public Game() {
    }

    public Game(User user, String playerWord, String computerWord) {
        playerGuesses = new ArrayList<>();
        computerGuesses = new ArrayList<>();
        correctChar = new ArrayList<>();
        incorrectChar = new ArrayList<>();
        computerCorrectChar = new ArrayList<>();
        computerIncorrectChar = new ArrayList<>();
        status = Status.IN_PROGRESS;

        this.user = user;
        this.playerWord = playerWord;
        this.computerWord = computerWord;
    }

    public enum Status {
        IN_PROGRESS,
        WIN,
        LOSS
    }


    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public Long getId() {
        return id;
    }

    public List<String> getPlayerGuesses() {
        return playerGuesses;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setPlayerGuesses(ArrayList<String> playerGuesses) {
        this.playerGuesses = playerGuesses;
    }

    public void addPlayerGuess(String guess) {
        playerGuesses.add(guess);
    }

    public void removePlayerGuess(String guess) {
        playerGuesses.remove(guess);
    }

    public List<String> getComputerGuesses() {
        return computerGuesses;
    }

    public List<Guess> getComputerGuessList() {
        return getGuesses(computerGuesses, playerWord);
    }

    private List<Guess> getGuesses(List<String> playerGuesses, String computerWord) {
        ArrayList<Guess> returnVal = new ArrayList<>();
        for (String g : playerGuesses) {
            returnVal.add(new Guess(g, evaluateGuess(g, computerWord)));
        }
        return returnVal;
    }

    public List<Guess> getPlayerGuessList() {
        return getGuesses(playerGuesses, computerWord);
    }

    public void setComputerGuesses(ArrayList<String> computerGuesses) {
        this.computerGuesses = computerGuesses;
    }

    public void addComputerGuess(String guess) {
        computerGuesses.add(guess);
    }

    public void removeComputerGuess(String guess) {
        computerGuesses.remove(guess);
    }

    public String getPlayerWord() {
        return playerWord;
    }

    public void setPlayerWord(String playerWord) {
        this.playerWord = playerWord;
    }

    public String getComputerWord() {
        return computerWord;
    }

    public void setComputerWord(String computerWord) {
        this.computerWord = computerWord;
    }

    public List<Character> getCorrectCharArray() {
        return correctChar;
    }

    public void setCorrectCharArray(ArrayList<Character> correctChar) {
        this.correctChar = correctChar;
    }

    public void addCorrectChar(Character newChar) {
        correctChar.add(newChar);
    }

    public void removeCorrectChar(Character newChar) {
        correctChar.remove(newChar);
    }

    public List<Character> getIncorrectCharArray() {
        return incorrectChar;
    }

    public void setIncorrectCharArray(ArrayList<Character> incorrectChar) {
        this.incorrectChar = incorrectChar;
    }

    public void addIncorrectChar(Character newChar) {
        incorrectChar.add(newChar);
    }

    public void removeIncorrectChar(Character newChar) {
        incorrectChar.remove(newChar);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void addComputerCorrectChar(char c) {
        computerCorrectChar.add(c);

    }

    public void addComputerIncorrectChar(char c) {
        computerIncorrectChar.add(c);
    }

    public List<Character> getComputerCorrectChar() {
        return computerCorrectChar;
    }

    public List<Character> getComputerIncorrectChar() {
        return computerIncorrectChar;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public static int evaluateGuess(String guess, String correctWord) {
        //Find out how many matching letters there are.
        int matching = 0;
        for (char c : guess.toCharArray()) {
            if (correctWord.contains(Character.toString(c))) matching++;
        }
        return matching;
    }

}
