package jotto.model;

public class Guess {
    public final String guess;
    public final int matchingChar;
    public Guess(String guess, int matchingChar) {
        this.guess = guess;
        this.matchingChar = matchingChar;
    }
}
