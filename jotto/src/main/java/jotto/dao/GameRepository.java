package jotto.dao;

import jotto.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

// TODO Chris H: Implement methods in class called UserRepositoryImpl
public interface GameRepository extends JpaRepository<Game, Long> {
    ArrayList<Game> findByUserUsername(String username);
}