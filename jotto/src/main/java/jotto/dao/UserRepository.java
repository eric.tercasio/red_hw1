package jotto.dao;

import jotto.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

// TODO Chris H: Implement methods in class called UserRepositoryImpl
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}