package jotto.controller;

import jotto.dao.GameRepository;
import jotto.dao.UserRepository;
import jotto.form.CreateGameForm;
import jotto.form.GuessForm;
import jotto.form.LetterStateForm;
import jotto.model.*;
import jotto.services.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.lang.String;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api")

public class GameplayController {

    private final UserRepository userRepository;

    private final GameRepository gameRepository;

    private final DictionaryService dictionaryService;

    @Autowired
    public GameplayController(UserRepository userRepository, GameRepository gameRepository, DictionaryService dictionaryService) {
        this.userRepository = userRepository;
        this.gameRepository = gameRepository;
        this.dictionaryService = dictionaryService;
    }

    @ExceptionHandler(value = HttpStatusCodeException.class)
    public ResponseEntity<String> handleException(HttpStatusCodeException e) {
        return new ResponseEntity<>(e.getStatusText(), e.getStatusCode());
    }

    @GetMapping
    public ArrayList<Game> getGames(Principal user) {
        return gameRepository.findByUserUsername(user.getName());
    }

    @GetMapping("/{gid}")
    public Game getGame(Principal user, @PathVariable Long gid) throws HttpStatusCodeException {
//        Inputs: User ID, game ID
//        Returns: game from database with that ID if game belongs to the user
        Game game = gameRepository.getOne(gid);
        if (game.getUser().getUsername().equals(user.getName()))
            return game;
        else
            throw new HttpServerErrorException(HttpStatus.FORBIDDEN, "This isn't your game");
    }

    @GetMapping("/dictionary")
    public boolean checkIfValidWord(String word) {
        return dictionaryService.contains(word);
    }

    @PostMapping
    public Game createGame(Principal user, @RequestBody CreateGameForm data) throws HttpStatusCodeException {
        String userWord = data.getWord().toLowerCase();
        HashSet<String> words = dictionaryService.getWords();
        if (checkStringDups(userWord)) {
            throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE, "Word must not have repeating letters");
        }
        if (!words.contains(userWord)) {
            throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE, "Word not in dictionary");
        }
        HashSet<String> easyWords = dictionaryService.getEasyWords();
        ArrayList<String> easyWordsArray = new ArrayList<>(easyWords);
        String computerWord = null;
        while (computerWord == null) {
            String chosenWord = easyWordsArray.get((int) (Math.random() * easyWordsArray.size()));
            if (!checkStringDups(chosenWord)) {
                computerWord = chosenWord;
            }
        }
        User user1 = userRepository.findByUsername(user.getName());

        Game game = new Game(user1, userWord.toLowerCase(), computerWord.toLowerCase());
        gameRepository.save(game);
        return game;
    }

    @PutMapping
    public Game submitGuess(Principal user, @RequestBody GuessForm guessData) throws HttpStatusCodeException {
        // Does: saves user guess, makes new computer guess (Eric & Chris)
        // Make sure game is in session before running that routine
        // Returns: game object without computer's chosen word
        // Inputs: User ID, user guess

        // First we need to obtain the UserID;
        Long gameId = guessData.getGid();
        String guess = guessData.getGuess().toLowerCase();
        Game currentGame = getGame(user, gameId);


        // Checks if the word is in the dictionary
        if (currentGame.getPlayerGuesses().contains(guess)) {
            throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE, "You already guessed that word");
        }
        if (!dictionaryService.contains(guess)) {
            throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE, "Word not in dictionary");

        }

        String computerWord = currentGame.getComputerWord();
        String playerWord = currentGame.getPlayerWord();

        currentGame.addPlayerGuess(guess);

        if (guess.equals(computerWord)) {
            currentGame.setStatus(Game.Status.WIN);
        }

        // Do computer guess
        HashSet<String> words = dictionaryService.getWords();

        List<Guess> computerGuessedWords = currentGame.getComputerGuessList();

        String computerGuess = calculateComputerGuess(words, playerWord, computerGuessedWords);
        if (computerGuess == null) {
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "The computer couldn't guess a word");

        }
        currentGame.addComputerGuess(computerGuess);

        if (computerGuess.equals(currentGame.getPlayerWord())) {
            currentGame.setStatus(Game.Status.LOSS);
        }

        gameRepository.save(currentGame);
        return currentGame;

    }

    @PostMapping("/letter")
    public Game changeLetterState(Principal user, @RequestBody LetterStateForm letterState) {
        Long gameId = letterState.getGameId();
        Character letter = letterState.getLetter();
        String state = letterState.getState();
        Game currentGame = getGame(user, gameId);

        switch (state) {
            case "correct":
                currentGame.addCorrectChar(letter);
                currentGame.removeIncorrectChar(letter);
                break;
            case "incorrect":
                currentGame.addIncorrectChar(letter);
                currentGame.removeCorrectChar(letter);
                break;
            default:
                currentGame.removeCorrectChar(letter);
                currentGame.removeIncorrectChar(letter);
        }

        gameRepository.save(currentGame);
        return currentGame;
    }

    private String calculateComputerGuess(HashSet<String> dictionary, String playerWord, List<Guess> guesses) {
        ArrayList<String> unguessedWords = new ArrayList<>(dictionary);
        for (Guess guess : guesses) {
            unguessedWords.removeIf((word) -> Game.evaluateGuess(word, playerWord) < guess.matchingChar || word.equals(guess.guess));
        }

        if (unguessedWords.size() == 0) {
            return null;
        }

        Random rand = new Random();
        return unguessedWords.get(rand.nextInt(unguessedWords.size()));
    }

    private static Boolean checkStringDups(String word) {
        char[] inp = word.toCharArray();
        for (int i = 0; i < word.length(); i++) {
            for (int j = i + 1; j < word.length(); j++) {
                if (inp[i] == inp[j]) {
                    return true;
                }
            }
        }
        return false;
    }

}
