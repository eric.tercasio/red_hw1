package jotto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SuppressWarnings("SameReturnValue")
@Controller
public class GameController {
    @GetMapping("/")
    public String index() {
        return "redirect:/game";
    }

    @GetMapping("/game/**")
    public String getGameUI() {
        return "game";
    }
}