package jotto.controller;

import jotto.dao.UserRepository;
import jotto.form.SignupForm;
import jotto.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.nio.CharBuffer;
import java.security.Principal;
import java.util.Arrays;

@SuppressWarnings("SameReturnValue")
@Controller
public class LoginController {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


    @Autowired
    public LoginController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/signup")
    public String postSignup(@ModelAttribute SignupForm form, RedirectAttributes attributes) {
        User existing = userRepository.findByUsername(form.getUsername());
        if (existing != null) {
            attributes.addFlashAttribute("error", "Account already exists");
            return "redirect:/login";
        }

        if (form.getPassword().length < 5) {
            attributes.addFlashAttribute("error", "Password must be more than 5 characters");
            return "redirect:/login";
        }

        if (!Arrays.equals(form.getPassword(), form.getPasswordConfirmation())) {
            attributes.addFlashAttribute("error", "Passwords don't match");
            return "redirect:/login";
        }

        CharSequence passwordSeq = CharBuffer.wrap(form.getPassword());
        String hash = encoder.encode(passwordSeq);

        User user = new User();
        user.setUsername(form.getUsername());
        user.setPassword(hash);
        userRepository.save(user);

        attributes.addFlashAttribute("error", "Please login with your new account!");
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String getLogin(Principal principal, RedirectAttributes attributes) {
        if (principal != null) {
            attributes.addFlashAttribute("error", "Already logged in");
            return "redirect:/";
        }

        return "login";
    }

}
