package jotto.form;

public class SignupForm {
    private String username;
    private char[] password;
    private char[] passwordConfirmation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public char[] getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(char[] passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
