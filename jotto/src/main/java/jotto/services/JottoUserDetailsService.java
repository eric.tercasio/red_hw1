package jotto.services;

import jotto.dao.UserRepository;
import jotto.model.JottoUserPrincipal;
import jotto.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JottoUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public JottoUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new JottoUserPrincipal(user);
    }


}