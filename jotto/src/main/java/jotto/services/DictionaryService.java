package jotto.services;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

@Service
public class DictionaryService {
    private HashSet<String> words;
    private HashSet<String> easyWords;

    public DictionaryService() throws IOException {
        Resource dict = new ClassPathResource("dictionary.txt");
        Resource easyDist = new ClassPathResource("alt_dictionary.txt");
        Scanner scanner = new Scanner(dict.getInputStream());
        Scanner easyScanner = new Scanner(easyDist.getInputStream());
        words = new HashSet<>();
        easyWords = new HashSet<>();
        while (scanner.hasNextLine()) {
            String word = scanner.nextLine();
            if (word.length() == 5 && word.matches("[a-zA-Z]+")) {
                words.add(word.toLowerCase());
            }
        }
        while (easyScanner.hasNextLine()) {
            String word = easyScanner.nextLine();
            if (word.length() == 5 && word.matches("[a-zA-Z]+")) {
                easyWords.add(word.toLowerCase());
                words.add(word);
            }
        }
    }

    public HashSet<String> getWords() {
        return words;
    }

    public HashSet<String> getEasyWords() {return easyWords;}

    public boolean contains(String word) {
        return words.contains(word.toLowerCase());
    }

    public boolean easyContains(String word){
        return easyWords.contains(word.toLowerCase());
    }
}
